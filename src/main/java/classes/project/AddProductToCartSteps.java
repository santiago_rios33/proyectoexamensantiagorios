package classes.project;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.testng.Assert;


public class AddProductToCartSteps
{
    Configuration myConf = new Configuration();
    AddProductToCart myAdd = new AddProductToCart();

    @When("Product image is present {string}")
    public void productImageIsPresentProductImage(String productImage)
    {
        myConf.isElementPresent(By.xpath(productImage));
    }

    @When("Tomato image is present {string}")
    public void tomatoImageIsPresent(String tomatoImage)
    {
        myConf.isElementPresent(By.xpath(myAdd.tomatoImage));
    }

    @Then("Validate that product is present in cart {string}")
    public void productImageIsPresent(String productCart)
    {
        boolean isPresent = myConf.isElementPresent(By.xpath(productCart));
        Assert.assertTrue(isPresent, "The product was added to the cart wrong.");
    }

    @Then("Validate that image of empty cart is present {string}")
    public void validateThatImageOfEmptyCartIsPresent(String emptyImage)
    {
        boolean isPresent = myConf.isElementPresent(By.xpath(myAdd.cartEmptyImage));
        Assert.assertTrue(isPresent, "The empty image of the cart is not present.");
    }

    @Then("Validate that price is correct for the number of tomatoes {string}")
    public void validateThatPriceIsCorrectForTheNumberOfTomatoes(String productCount)
    {
        String basePriceText = myConf.getText(By.xpath("(//p[@class='product-price'][contains(.,'16')])[1]"));
        double basePrice = Double.parseDouble(basePriceText.replace("$", "").trim());

        String quantityText = myConf.getText(By.xpath("(//p[@class='quantity'][contains(.,'2 Nos.')])[1]"));
        int quantity = Integer.parseInt(quantityText.replace(" Nos.", "").trim());

        String totalPriceText = myConf.getText(By.xpath("(//p[@class='amount'][contains(.,'32')])[1]"));
        double totalPrice = Double.parseDouble(totalPriceText.replace("$", "").trim());

        double expectedPrice = basePrice * quantity;

        Assert.assertEquals(totalPrice, expectedPrice, "The total price does not match the number of tomatoes multiplied by the base price.");
    }

    @Then("Validate that price is correct for the number of tomatoes in the cart preview {string}")
    public void validateThatPriceIsCorrectForTheNumberOfTomatoesInTheCartPreview(String productCount)
    {
        String basePriceText = myConf.getText(By.xpath("(//p[@class='product-price'][contains(.,'16')])[2]"));
        double basePrice = Double.parseDouble(basePriceText.replace("$", "").trim());

        String quantityText = myConf.getAttributeValue(By.xpath("//input[contains(@value,'2')]"), "value");
        int quantity = Integer.parseInt(quantityText.trim());

        String totalPriceText = myConf.getText(By.xpath("//strong[contains(.,'32')]"));
        double totalPrice = Double.parseDouble(totalPriceText.replace("$", "").trim());

        double expectedPrice = basePrice * quantity;

        Assert.assertEquals(totalPrice, expectedPrice, "The total price does not match the number of tomatoes multiplied by the base price.");
    }
}
