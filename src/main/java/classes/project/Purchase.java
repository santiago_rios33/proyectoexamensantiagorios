package classes.project;

public class Purchase
{
    String proceedToCheckout = "//button[@type='button'][contains(.,'PROCEED TO CHECKOUT')]";
    String placeOrderButton = "//button[contains(.,'Place Order')]";
    String countryDropDown = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]";
    String checkBox = "//input[contains(@type,'checkbox')]";
    String proceed = "//button[contains(.,'Proceed')]";
    String homeButton = "//a[@href='#/'][contains(.,'Home')]";
    String promoTextBox = "//input[contains(@class,'promoCode')]";
    String applyButton = "//button[@class='promoBtn'][contains(.,'Apply')]";
    String searchBar = "//input[contains(@type,'search')]";
    String cart = "//img[contains(@alt,'Cart')]";
}
