package classes.project;

public class AddProductToCart
{
    String cart = "//img[contains(@alt,'Cart')]";
    String addToCart = "//button[@type='button'][contains(.,'ADD TO CART')]";
    String cartEmptyImage = "(//img[contains(@alt,'empty-cart')])[1]";
    String removeButton = "(//a[contains(@class,'product-remove')])[1]";
    String tomatoImage = "//img[@src='./images/tomato.jpg']";
    String incrementButton = "(//a[contains(@class,'increment')])[1]";
}
