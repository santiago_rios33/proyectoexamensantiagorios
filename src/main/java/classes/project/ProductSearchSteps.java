package classes.project;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.testng.Assert;

public class ProductSearchSteps
{
    Configuration myConf = new Configuration();
    ProductSearch myProductSearch = new ProductSearch();
    AddProductToCart myAddProductToCart = new AddProductToCart();
    Purchase myPurchase = new Purchase();
    ValidationOfTopDealsFiltering myVTD = new ValidationOfTopDealsFiltering();

    @Given("Click on button {string}")
    public void darClickEnBoton(String button)
    {
        switch (button)
        {
            case "searchBar": myConf.clickElement(myProductSearch.searchBar);
            break;
            case "cart": myConf.clickElement(myAddProductToCart.cart);
            break;
            case "addToCart": myConf.clickElement(myAddProductToCart.addToCart);
            break;
            case "searchButton": myConf.clickElement(myProductSearch.searchButton);
            break;
            case "removeButton": myConf.clickElement(myAddProductToCart.removeButton);
            break;
            case "incrementButton": myConf.clickElement(myAddProductToCart.incrementButton);
            break;
            case "proceedToCheckout": myConf.clickElement(myPurchase.proceedToCheckout);
            break;
            case "placeOrderButton": myConf.clickElement(myPurchase.placeOrderButton);
            break;
            case "countryDropDown": myConf.clickElement(myPurchase.countryDropDown);
            break;
            case "checkBox": myConf.clickElement(myPurchase.checkBox);
            break;
            case "proceed": myConf.clickElement(myPurchase.proceed);
            break;
            case "promoTextBox": myConf.clickElement(myPurchase.promoTextBox);
            break;
            case "applyButton": myConf.clickElement(myPurchase.applyButton);
            break;
            case "searchTopDealsButton": myConf.clickElement(myVTD.searchTopDealsButton);
            break;
            case "productsDropDown": myConf.clickElement(myVTD.productsDropDown);
            break;
            case "nextButton": myConf.clickElement(myVTD.nextButton);
            break;
            case "lastButton": myConf.clickElement(myVTD.lastButton);
        }
    }

    @And("Search a product {string}")
    public void searchAProduct(String product) throws InterruptedException
    {
        myConf.write(myProductSearch.searchBar,product);
        Thread.sleep(2000);
    }

    @Then("Validate that the correct product image is present {string}")
    public void productImageIsPresent(String productImage)
    {
        boolean isPresent = myConf.isElementPresent(By.xpath(productImage));
        Assert.assertTrue(isPresent, "The image not matches with the searched product.");
    }

    @Then("Validate that the correct message appears {string}")
    public void validateThatTheCorrectMessageAppears(String message)
    {
        boolean correctMessage = myConf.isElementPresent(By.xpath(message));
        Assert.assertTrue(correctMessage, "The message is not correct.");
    }

    @Then("Validate that the correct product image with that letter is present {string}")
    public void validateThatTheCorrectProductImageWithThatLetterIsPresent(String product)
    {
        boolean almondsImage = myConf.isElementPresent(By.xpath(product));
        Assert.assertTrue(almondsImage, "The product is not correct.");
    }
}
