package classes.project;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class Configuration
{

    protected static WebDriver driver;
    private static WebDriverWait wait2;
    protected static Actions action;

    // Constructor de la clase
    @Before
    public void setUp()
    {
        if (driver == null)
        {
            ChromeOptions options = new ChromeOptions();
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            wait2 = new WebDriverWait(driver, Duration.ofSeconds(10));
            action = new Actions(driver);
        }
    }

    @After
    public void closePag()
    {
        if (driver != null)
        {
            driver.quit();
            driver = null;
        }
    }

    @Given("Access the page {string}")
    public void accessthePag(String url)
    {
        driver.get(url);

    }

    public void clickElement(String locator)
    {
        if (findByXpath(locator).isDisplayed())
        {
            findByXpath(locator).click();
        }
    }

    public boolean isElementPresent(By locator)
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return true;
        }
        catch (NoSuchElementException e)
        {
            return true; // Element is not present
        }
        catch (Exception e)
        {
            return false; // Element might be present or some other issue occurred
        }
    }


    public void write(String locator, String textToWrite)
    {
        findByXpath(locator).clear();
        findByXpath(locator).sendKeys(textToWrite);
    }


    public static WebElement findByXpath(String locator)
    {
        return wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public String getText(By locator)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return element.getText();
    }

    public String getAttributeValue(By locator, String attribute)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return element.getAttribute(attribute);
    }
}
