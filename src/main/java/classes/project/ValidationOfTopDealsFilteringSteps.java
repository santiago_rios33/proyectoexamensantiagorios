package classes.project;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import static classes.project.Configuration.driver;

public class ValidationOfTopDealsFilteringSteps
{
    Configuration myConf = new Configuration();
    ValidationOfTopDealsFiltering myVTD = new ValidationOfTopDealsFiltering();
    int basePrice;
    int discountPrice;

    @And("Product basePrice is present {string}")
    public void productBasePriceIsPresent(String productBasePrice)
    {
        WebElement element = driver.findElement(By.xpath(productBasePrice));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        basePrice = Integer.parseInt(numericPriceText);
    }

    @And("Product discountPrice is present {string}")
    public void productDiscountPriceIsPresent(String productDiscountPrice)
    {
        WebElement element = driver.findElement(By.xpath(productDiscountPrice));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        discountPrice = Integer.parseInt(numericPriceText);
    }

    @Then("Validate that the difference is equivalent to the price of the product")
    public void validateThatTheDifferenceIsEquivalentToThePriceOfTheProduct()
    {
        int discountedPrice = basePrice - discountPrice;
        Assert.assertEquals(myVTD.tomatoPrice, discountedPrice, "The total price is not correct. Expected: " + myVTD.tomatoPrice + ", but was: " + discountedPrice);
    }

    @Then("Validate that the product exists in the list {string}")
    public void validateThatTheProductExistsInTheList(String product)
    {
        boolean isProductPresent = myConf.isElementPresent(By.xpath(product));
        Assert.assertTrue(isProductPresent, "The product does not appear.");
    }

    @And("Choose the option {string}")
    public void chooseTheOption(String option) throws InterruptedException
    {
        WebElement dropdownElement = driver.findElement(By.xpath(myVTD.productsDropDown));
        Select dropdown = new Select(dropdownElement);
        dropdown.selectByVisibleText(option);
    }
}
