package classes.project;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

import static classes.project.Configuration.driver;

public class PurchaseSteps
{
    Configuration myConf = new Configuration();
    Purchase myPurchase = new Purchase();
    String currentUrl;

    @When("Choose the country {string}")
    public void chooseTheCountry(String countryName)
    {
        WebElement dropdownElement = driver.findElement(By.xpath(myPurchase.countryDropDown));
        Select dropdown = new Select(dropdownElement);
        dropdown.selectByVisibleText(countryName);
    }

    @Then("Validate that the completed purchase Home button appears {string}")
    public void validateThatTheCompletedPurchaseHomeButtonAppears(String home)
    {
        boolean homeButton = myConf.isElementPresent(By.xpath(myPurchase.homeButton));
        Assert.assertTrue(homeButton, "The purchase failed.");
    }

    @And("Write the code {string}")
    public void writeTheCode(String code) throws InterruptedException
    {
        myConf.write(myPurchase.promoTextBox, code);
        Thread.sleep(2000);
    }

    @Given("Get current url")
    public void getCurrentUrl() throws InterruptedException
    {
        currentUrl = driver.getCurrentUrl();
        System.out.println("La URL actual es: " + currentUrl);
    }

    @Then("Validate that the URL is not the cart URL after proceeding to checkout {string}")
    public void validateThatTheURLIsNotTheCartURLAfterProceedingToCheckout(String url)
    {
        Assert.assertEquals(url, currentUrl, "The URL is not correct.");
    }

    @Then("Validate that the completed purchase Home button not appears {string}")
    public void validateThatTheCompletedPurchaseHomeButtonNotAppears(String home)
    {
        boolean homeButton = myConf.isElementPresent(By.xpath(myPurchase.homeButton));
        Assert.assertFalse(homeButton, "The purchase was completed with incomplete data.");
    }
}
