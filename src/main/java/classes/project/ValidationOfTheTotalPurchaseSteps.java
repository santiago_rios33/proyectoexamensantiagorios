package classes.project;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

import static classes.project.Configuration.driver;

public class ValidationOfTheTotalPurchaseSteps
{
    Configuration myConf = new Configuration();
    int totalPrice1;
    int productPrice;
    int priceProduct1;
    int priceProduct2;
    int totalPrice2;
    int quantityOfProducts;

    @And("Product totalPrice is present {string}")
    public void productTotalPriceIsPresent(String price)
    {
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(price)));
        String priceText = element.getText().trim();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        productPrice = Integer.parseInt(numericPriceText);
    }

    @When("Price is present {string}")
    public void priceIsPresent(String price)
    {
        WebElement element = driver.findElement(By.xpath(price));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        totalPrice1 = Integer.parseInt(numericPriceText);
    }

    @Then("Validate that prices are equal")
    public void validateThatPricesAreEqual()
    {
        Assert.assertEquals(productPrice, totalPrice1, "The total price is not correct. Expected: " + productPrice + ", but was: " + totalPrice1);
    }

    @When("Price of tomato is present {string}")
    public void priceOfProductIsPresent(String price)
    {
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(price)));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        priceProduct1 = Integer.parseInt(numericPriceText);
    }

    @And("Price of carrot is present {string}")
    public void priceOfCarrotIsPresent(String price)
    {
        WebElement element = driver.findElement(By.xpath(price));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        priceProduct2 = Integer.parseInt(numericPriceText);
    }

    @And("Products totalPrice is present {string}")
    public void productsTotalPriceIsPresent(String price)
    {
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(price)));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        totalPrice2 = Integer.parseInt(numericPriceText);
    }

    @Then("Validate that the sum of the prices is equivalent")
    public void validateThatTheSumOfThePricesIsEquivalent()
    {
        int expectedPrice = priceProduct1 + priceProduct2;
        Assert.assertEquals(totalPrice2, expectedPrice, "The total price is not correct. Expected: " + expectedPrice + ", but was: " + totalPrice2);
    }

    @And("Total quantity of product is present {string}")
    public void totalQuantityOfProductIsPresent(String quantity)
    {
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(quantity)));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        quantityOfProducts = Integer.parseInt(numericPriceText);
    }

    @Then("Validate that the total price is correct")
    public void validateThatTheTotalPriceIsCorrect()
    {
        int expectedPrice = priceProduct1 * quantityOfProducts;
        Assert.assertEquals(totalPrice2, expectedPrice, "The total price is not correct. Expected: " + expectedPrice + ", but was: " + totalPrice2);
    }

    @And("Total price of home page is present {string}")
    public void totalPriceOfHomePageIsPresent(String price)
    {
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(price)));
        String priceText = element.getText();
        String numericPriceText = priceText.replaceAll("[^0-9]", "");
        totalPrice1 = Integer.parseInt(numericPriceText);
    }

    @Then("Validate that the totals are equal")
    public void validateThatTheTotalsAreEqual()
    {
        Assert.assertEquals(totalPrice1, totalPrice2, "The total price is not correct");
    }
}
