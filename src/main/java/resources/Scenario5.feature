Feature: Scenario5

  Background:
    Given Access the page "https://rahulshettyacademy.com/seleniumPractise/#/offers"

  @ValidationOfTopDealsFiltering
  Scenario: ValidateThePriceOfTomato
    Given Click on button "searchTopDealsButton"
    And Search a product "tomato"
    And Product basePrice is present "(//td[contains(.,'37')])[1]"
    And Product discountPrice is present "//td[contains(.,'26')]"
    Then Validate that the difference is equivalent to the price of the product

  Scenario: DisplayAllProducts
    Given Click on button "productsDropDown"
    And Choose the option "20"
    Then Validate that the product exists in the list "//td[contains(.,'Almond')]"

  Scenario: ProductNonexistent
    Given Click on button "searchBar"
    And Search a product "lemon"
    Then Validate that the correct message appears "//td[@colspan='3'][contains(.,'No data')]"

  Scenario: SearchProductOnNextPage
    Given Click on button "nextButton"
    Then Validate that the product exists in the list "//td[contains(.,'Mango')]"

  Scenario: SearchProductOnLastPage
    Given Click on button "lastButton"
    Then Validate that the product exists in the list "//td[contains(.,'Apple')]"



