Feature: Scenario3

  Background:
    Given Access the page "https://rahulshettyacademy.com/seleniumPractise/#/"

  @Purchase
  Scenario Outline: PurchaseOfMultipleProducts
    Given Click on button "searchBar"
    And Search a product "<Product>"
    When Product image is present <ProductImage>
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    And Click on button "placeOrderButton"
    And Click on button "countryDropDown"
    When Choose the country "Uruguay"
    And Click on button "checkBox"
    And Click on button "proceed"
    Then Validate that the completed purchase Home button appears "homeButton"
    Examples:
      | Product | ProductImage                         |
      | Tomato  | "//img[@src='./images/tomato.jpg']"  |
      | Potato  | "//img[@src='./images/potato.jpg']"  |
      | Walnuts | "//img[@src='./images/walnuts.jpg']" |

  Scenario: InvalidPromoCode
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    And Click on button "promoTextBox"
    And Write the code "promo"
    When Click on button "applyButton"
    Then Validate that the correct message appears "//span[@class='promoInfo'][contains(.,'Invalid code ..!')]"

  Scenario: PurchaseWithoutAcceptingTheTermsAndConditions
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    And Click on button "placeOrderButton"
    And Click on button "countryDropDown"
    When Choose the country "Uruguay"
    And Click on button "proceed"
    Then Validate that the correct message appears "//b[contains(.,'Please accept Terms & Conditions - Required')]"

  Scenario: MakeAPurchaseWithoutProducts
    Given Get current url
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    Then Validate that the URL is not the cart URL after proceeding to checkout "https://rahulshettyacademy.com/seleniumPractise/#/cart"

  Scenario: MakeAPurchaseWithoutChoosingTheCountry
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    And Click on button "placeOrderButton"
    And Click on button "checkBox"
    And Click on button "proceed"
    Then Validate that the completed purchase Home button not appears "homeButton"