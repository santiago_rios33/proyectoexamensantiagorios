Feature: Scenario1

  Background:
    Given Access the page "https://rahulshettyacademy.com/seleniumPractise/#/"

 @ProductSearch
  Scenario Outline: SearchExistingProducts
    Given Click on button "searchBar"
    And Search a product "<Product>"
    Then Validate that the correct product image is present <ProductImage>
    Examples:
      | Product | ProductImage                         |
      | Tomato  | "//img[@src='./images/tomato.jpg']"  |
      | Potato  | "//img[@src='./images/potato.jpg']"  |
      | Walnuts | "//img[@src='./images/walnuts.jpg']" |

  Scenario: ProductNonexistent
    Given Click on button "searchBar"
    And Search a product "garlic"
    Then Validate that the correct message appears "//h2[contains(.,'Sorry, no products matched your search!')]"

  Scenario: SearchWithALetter
    Given Click on button "searchBar"
    And Search a product "g"
    Then Validate that the correct product image with that letter is present "//h4[@class='product-name'][contains(.,'Almonds - 1/4 Kg')]"


  Scenario: EmptySearch
    Given Click on button "searchBar"
    And Click on button "search"
    Then Validate that the correct message appears "//h2[contains(.,'Sorry, no products matched your search!')]"

  Scenario: SearchWithSpecialCharacters
    Given Click on button "searchBar"
    And Search a product "+"
    Then Validate that the correct message appears "//h2[contains(.,'Sorry, no products matched your search!')]"