Feature: Scenario2

  Background:
    Given Access the page "https://rahulshettyacademy.com/seleniumPractise/#/"

    @AddProductToCart
  Scenario Outline: AddProduct
    Given Click on button "searchBar"
    And Search a product "<Product>"
    When Product image is present <ProductImage>
    And Click on button "addToCart"
    And Click on button "cart"
    Then Validate that product is present in cart <ProductCart>
    Examples:
      | Product | ProductCart                                                        | ProductImage                         |
      | Tomato  | "(//p[@class='product-name'][contains(.,'Tomato - 1 Kg')])[1]"     | "//img[@src='./images/tomato.jpg']"  |
      | Potato  | "(//p[@class='product-name'][contains(.,'Potato - 1 Kg')])[1]"     | "//img[@src='./images/potato.jpg']"  |
      | Walnuts | "(//p[@class='product-name'][contains(.,'Walnuts - 1/4 Kg')])[1]"  | "//img[@src='./images/walnuts.jpg']" |

  Scenario: CartEmpty
    Given Click on button "cart"
    Then Validate that image of empty cart is present "cartEmptyImage"

  Scenario Outline: RemoveProduct
    Given Click on button "searchBar"
    And Search a product "<Product>"
    When Product image is present <ProductImage>
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "removeButton"
    Then Validate that image of empty cart is present "cartEmptyImage"
    Examples:
      | Product | ProductImage                         |
      | Tomato  | "//img[@src='./images/tomato.jpg']"  |
      | Potato  | "//img[@src='./images/potato.jpg']"  |
      | Walnuts | "//img[@src='./images/walnuts.jpg']" |

  Scenario: AddMultipleItemsOfTheSameProduct_1
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Tomato image is present "tomatoImage"
    And Click on button "incrementButton"
    And Click on button "addToCart"
    And Click on button "cart"
    Then Validate that price is correct for the number of tomatoes "number"

  Scenario: AddMultipleItemsOfTheSameProduct_2
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Tomato image is present "tomatoImage"
    And Click on button "incrementButton"
    And Click on button "addToCart"
    Then Validate that price is correct for the number of tomatoes in the cart preview "number"
