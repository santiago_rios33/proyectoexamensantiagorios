Feature: Scenario4

  Background:
    Given Access the page "https://rahulshettyacademy.com/seleniumPractise/#/"

  @ValidationOfTheTotalPurchase
  Scenario: TotalPriceOfAProduct
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    When Price is present "//p[@class='product-price'][contains(.,'16')]"
    And Product totalPrice is present "(//p[@class='amount'][contains(.,'16')])[2]"
    Then Validate that prices are equal

  Scenario: TotalPriceOfTwoDifferentProducts
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "addToCart"
    And Click on button "searchBar"
    And Search a product "carrot"
    When Product image is present "//img[@src='./images/carrots.jpg']"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    When Price of tomato is present "//p[@class='product-price'][contains(.,'16')]"
    And Price of carrot is present "(//p[@class='amount'][contains(.,'56')])[1]"
    And Products totalPrice is present "//span[@class='totAmt'][contains(.,'72')]"
    Then Validate that the sum of the prices is equivalent

  Scenario: TotalPriceOfAQuantityOfAProduct
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "incrementButton"
    And Click on button "addToCart"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    When Price of tomato is present "//p[@class='amount'][contains(.,'16')]"
    And Total quantity of product is present "//p[@class='quantity'][contains(.,'2')]"
    And Products totalPrice is present "//span[@class='totAmt'][contains(.,'32')]"
    Then Validate that the total price is correct

  Scenario: ValidateThatTheTotalsMatch
    Given Click on button "searchBar"
    And Search a product "tomato"
    When Product image is present "//img[@src='./images/tomato.jpg']"
    And Click on button "incrementButton"
    And Click on button "addToCart"
    And Total price of home page is present "//strong[contains(.,'32')]"
    And Click on button "cart"
    And Click on button "proceedToCheckout"
    And Products totalPrice is present "//span[@class='totAmt'][contains(.,'32')]"
    Then Validate that the totals are equal
